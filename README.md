## Opis

Program generujący zadania z matematyki dla uczniów, coś w przybliżeniu takiego jak [tutaj](https://www.matzoo.pl/).

Przykład:
```
Rodzaj działania: +
Ogranicz wynik: 100
Chcesz ograniczyć argumenty (N/t): n
Liczba rund: 3

Start!
12 + 15 = 27

Dobrze!
Poprawnie | Błędnie | Procent poprawnych | Do końca
1         | 0       | 100                | 2 rundy


8 + 1 = 10

Źle!
8 + 1 = 9
Poprawnie | Błędnie | Procent poprawnych | Do końca
1         | 1       | 50                 | 1 runda


50 + 20 = 70

Dobrze!
Poprawnie | Błędnie | Procent poprawnych | Do końca
2         | 1       | 66                 | 0 rund
```
## ToDo

1. Testy, testy, testy, ...
2. Przebieg gry.
3. Rodzaje zadań.
